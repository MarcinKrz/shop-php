<?php
session_start();
if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}
include("functions.php");

$sql = 'select * from Products';
$result = $conn->query($sql);
?>

<html>
<head>
    <title>PHP SHOP</title>

    <script type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css" title="Arkusz stylów CSS">


    <?php

    if (isset($_POST['clear'])) {
        session_unset();
        $_SESSION['cart'] = array();
        header("Location: shop.php");
    }
    if (isset($_POST['buy'])) {
        $conn->begin_transaction();
        $a = 1;
        foreach ($_SESSION['cart'] as $prod) {
            $sql = 'delete from Products where name = ?';
            $statement = $conn->prepare($sql);
            $statement->bind_param('s', $prod);
            $statement->execute();
            $a = $conn->affected_rows;
            if ($a == 0) {
                $conn->rollback();
                break;
            }
        }
        session_unset();
        $_SESSION['cart'] = array();
        if ($a == 0) {
//            echo $a;
            header("Location: shop.php?warning");
        } else {
            $conn->commit();
            header("Location: shop.php?success");
        }
    }

    ?>

</head>
<body>
<header>
    <h1>Sklep online</h1>
    <br>
</header>

<div class="flex-container">
    <div class="flex-item">
        <p>Lista dostępnych produktów</p>
        <table class="table" style="width: auto">
            <?php
            foreach ($result as $prod) { ?>
                <div class="product">
                    <tr>
                        <th><?= $prod["name"] ?></th>
                        <th>
                            <form  action='shop.php' method='post'>
                                <button class="addToCartButton" type='submit' name='name'
                                        value="<?= $prod["name"] ?>" style="display:inline">Do koszyka
                                </button>
                        </th>
                        </form>
                    </tr>
                </div>
            <?php } ?>
        </table>
    </div>
    <div class="flex-item">
        <p>Koszyk</p>
        <div id="products-in-basket">
            <table class="table">
            <?php
            if (isset($_POST['name'])) {
                if (!in_array($_POST['name'], $_SESSION['cart'])) {
                    array_push($_SESSION['cart'], $_POST['name']);
                    //                header("Location: shop.php");
                }
                foreach ($_SESSION['cart'] as $item) {
                    ?>
                    <tr>
                        <th><?=$item?></th>
                    </tr>
               <?php }
            }?>
            </table>
        </div>
        <div class="action-form">
            <form action='shop.php' method='post'>
                <button class="clearButton" type='submit' name='clear' value='true'>Wyczyść</button>
            </form>
            <form action='shop.php' method='post'>
                <button class="buyButton" type='submit' name='buy' value='true'>Zakup</button>
            </form>
        </div>
    </div>
</div>
<?php if (isset($_GET['warning'])) { ?>
    <div class="alert alert-danger">
        <strong>Uwaga!</strong> Niestety, zakup zakończył się nie pomyślnie
    </div>
<?php } ?>
<?php if (isset($_GET['success'])) { ?>
    <div class="alert alert-success">
        <strong>Brawo!</strong> Zakup został przeprowadzony pomyślnie.
    </div>
<?php }; ?>

</body>
</html>